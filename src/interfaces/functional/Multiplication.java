package interfaces.functional;

@FunctionalInterface
interface Multiplication
{
	double multiply(int i, double j);
}
