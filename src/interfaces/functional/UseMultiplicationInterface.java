package interfaces.functional;

/**
 * @author ben
 * @since 31/10/2019
 */
public class UseMultiplicationInterface
{
	public static void main(String[] args)
	{
		Multiplication func = (int i, double y) -> i * y;
		var multiplyResult = func.multiply(1, 3.0);
		System.out.println(multiplyResult);
	}
}
