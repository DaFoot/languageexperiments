package paths;

import java.nio.file.Path;

/**
 * @author ben
 * @since 31/10/2019
 */
public class Paths
{
	public static void main(String[] args)
	{
		Path p1 = Path.of("somewhere/somewhereelse");
		Path p2 = Path.of("somewhere/elsewhere");
		System.out.println(p1.resolveSibling(p1));
	}
}
