package beans;

/**
 * @author ben
 * @since 29/10/2019
 */
public class Lizard implements Reptile
{
	@Override
	public String noise()
	{
		return "hiss";
	}
}
