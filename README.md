# README #

This repo does not contain an application, but notes made and experiments done while studying for Java certification.

Some simple classes in beans package for playing with inheritance

Started out as code for Java 8 exam.
Now I'm starting J11 certification, new code may not run in Java 8.  


### Want to contribute? ###
If I've set permissions up correctly(!) you should be able to fork and create pull requests if you'd like to contribute.
Please DO NOT contribute any code copy/pasted from other sites, eg example exam questions
If  you want to contribute for later versions of Java (I will do too when I get round to doing Java 11 exam!), please note in code that a particular feature is from version x.

### Who do I talk to? ###
I can be reached via my website www.dafoot.co.uk
